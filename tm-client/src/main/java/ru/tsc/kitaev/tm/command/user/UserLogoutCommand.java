package ru.tsc.kitaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractCommand;

@Component
public class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "logout system...";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        sessionEndpoint.closeSession(sessionService.getSession());
        sessionService.setSession(null);
        System.out.println("[OK]");
    }

}
