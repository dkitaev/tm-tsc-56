package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveWithAllTasksByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-with-tasks-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (projectEndpoint.findProjectById(sessionService.getSession(), projectId) == null) throw new ProjectNotFoundException();
        projectTaskEndpoint.removeById(sessionService.getSession(), projectId);
    }

}
