package ru.tsc.kitaev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.endpoint.ProjectDTO;
import ru.tsc.kitaev.tm.endpoint.ProjectEndpoint;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;

@Component
public abstract class AbstractProjectCommand extends AbstractCommand{

    protected void showProject(@Nullable ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

}
