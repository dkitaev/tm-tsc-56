package ru.tsc.kitaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ArgumentsShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list arguments...";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands();
        for (@NotNull final AbstractCommand command : commands) {
            @Nullable String argument = command.arg();
            if (argument != null || !argument.isEmpty())
                System.out.println(argument + ": " + command.description());
        }
    }

}
