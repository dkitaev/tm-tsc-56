package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskFinishByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by name...";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        taskEndpoint.finishTaskByName(sessionService.getSession(), name);
    }

}
