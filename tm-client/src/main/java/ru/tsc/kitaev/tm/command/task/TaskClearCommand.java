package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks...";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        taskEndpoint.clearTask(sessionService.getSession());
        System.out.println("[OK]");
    }

}
