package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start task by index...";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        taskEndpoint.startTaskByIndex(sessionService.getSession(), index);
    }

}
