package ru.tsc.kitaev.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.api.service.*;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.constant.TerminalConst;
import ru.tsc.kitaev.tm.exception.system.UnknownCommandException;
import ru.tsc.kitaev.tm.service.*;
import ru.tsc.kitaev.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

@Getter
@Setter
@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initRegistry(abstractCommands);
        runArgs(args);
        initPID();
        logService.debug("Test environment");
        @NotNull final Scanner scanner = new Scanner(System.in);
        @NotNull String command = "";
        fileScanner.init();
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    @SneakyThrows
    private void initRegistry(@NotNull final AbstractCommand[] commands) {
        for (@NotNull final AbstractCommand command: commands) {
            registry(command);
        }
    }

    public void runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException();
        command.execute();
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        abstractCommand.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
