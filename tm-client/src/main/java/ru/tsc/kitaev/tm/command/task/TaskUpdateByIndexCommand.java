package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index...";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!taskEndpoint.existsTaskByIndex(sessionService.getSession(), index)) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @NotNull final String description = TerminalUtil.nextLine();
        taskEndpoint.updateTaskByIndex(sessionService.getSession(), index, name, description);
    }

}
