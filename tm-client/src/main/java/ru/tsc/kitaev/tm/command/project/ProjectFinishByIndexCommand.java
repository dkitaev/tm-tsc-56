package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish project by index...";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        projectEndpoint.finishProjectByIndex(sessionService.getSession(), index);
    }

}
