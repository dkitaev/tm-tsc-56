package ru.tsc.kitaev.tm.repository.model;

import lombok.AllArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.api.repository.model.ITaskRepository;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class TaskRepository extends AbstractRepository implements ITaskRepository {

    @Override
    public void add(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Override
    public void update(@NotNull final Task task) {
        entityManager.merge(task);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM Task WHERE user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @NotNull List<Task> findAll() {
        return entityManager
                .createQuery("FROM Task", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public @NotNull List<Task> findAllByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId AND id = :id", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public @NotNull Task findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findById(userId, id));
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findByIndex(userId, index));
    }

    @Override
    public @NotNull Integer getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(t) FROM Task t WHERE user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public @NotNull Task findByName(@NotNull final String userId, @NotNull final String name) {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId AND t.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        entityManager.remove(findByName(userId, name));
    }

    @Override
    public @NotNull List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId AND project.id = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        entityManager
                .createQuery("DELETE FROM Task WHERE user.id = :userId AND project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
