package ru.tsc.kitaev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.model.User;

import java.util.List;

public interface IUserService {

    void clear();

    @NotNull
    List<User> findAll();

    @Nullable
    User findById(@NotNull final String id);

    @NotNull
    User findByIndex(@NotNull final Integer index);

    void removeById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

    boolean existsByIndex(final int index);

    void addAll(@NotNull List<User> users);

    @Nullable
    User findByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    boolean isEmailExists(@Nullable String email);

    void removeByLogin(@Nullable String login);

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void setPassword(@Nullable String userId, @Nullable String password);

    void setPassword(@NotNull User user, @NotNull String password);

    void updateUser(@Nullable String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
