package ru.tsc.kitaev.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.api.service.*;
import ru.tsc.kitaev.tm.api.service.dto.*;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    public void start(@Nullable final String[] args) {
        try {
            BasicConfigurator.configure();
            initPID();
            initEndpoint(endpoints);
            //initUsers();
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.exit(1);
            }
    }

    private void initEndpoint(@NotNull final AbstractEndpoint[] endpoints) {
        for (@NotNull final AbstractEndpoint endpoint: endpoints) {
            registry(endpoint);
        }
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final Integer port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initUsers() {
            userService.create("test", "test");
            userService.create("admin", "admin", Role.ADMIN);
    }

}
