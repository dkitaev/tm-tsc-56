package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.kitaev.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @Override
    @Nullable
    @WebMethod
    public UserDTO findByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        return userService.findByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findByEmail(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        return userService.findByEmail(email);
    }

    @Override
    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @Override
    @NotNull
    @WebMethod
    public UserDTO createUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, email);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        userService.unlockUserByLogin(login);
    }

}
