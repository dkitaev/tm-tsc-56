package ru.tsc.kitaev.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.ILoggerService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.exception.user.UserNotFoundException;
import ru.tsc.kitaev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@AllArgsConstructor
public final class UserDTOService extends AbstractDTOService implements IUserDTOService {

    @NotNull
    public IUserDTORepository getRepository() {
        return context.getBean(IUserDTORepository.class);
    }

    @NotNull
    @Autowired
    private ILoggerService logService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    public void clear() {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findById(@NotNull final String id) {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findById(id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO findByIndex(@NotNull final Integer index) {
        if (index < 0) throw new EmptyIndexException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByIndex(index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @Nullable final UserDTO user = userRepository.findById(id);
            if (user == null) throw new UserNotFoundException();
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @Nullable final UserDTO user = userRepository.findById(id);
            return user != null;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIndex(final int index) {
        if (index < 0) throw new EmptyIndexException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            userRepository.findByIndex(index);
            return true;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final List<UserDTO> users) {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (UserDTO user : users) {
                userRepository.add(user);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByLogin(login) == null;
        } catch (@NotNull final Exception e) {
            return false;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByEmail(email);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByEmail(email) == null;
        } catch (@NotNull final Exception e) {
            return false;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @Nullable final UserDTO user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (!isLoginExists(login)) {
            System.out.printf("Такой пользователь %s уже существует%n", login);
            return null;
        }
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (!isLoginExists(login)) {
            System.out.printf("Такой пользователь %s уже существует%n", login);
            return null;
        }
        if (!isEmailExists(email)) {
            System.out.printf("Такой email %s уже существует%n", email);
            return null;
        }
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        user.setEmail(email);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (!isLoginExists(login)) {
            System.out.printf("Такой пользователь %s уже существует%n", login);
            return null;
        }
        if (role == null) throw new EmptyRoleException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        setPassword(user, password);
        user.setRole(role);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable final UserDTO user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        setPassword(user, password);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setPassword(@Nullable final UserDTO user, @Nullable final String password) {
        if (user == null) throw new EntityNotFoundException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        user.setPasswordHash(hash);
    }

    @Override
    public void updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable final UserDTO user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

}
