package ru.tsc.kitaev.tm.repository.model;

import lombok.AllArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.api.repository.model.ISessionRepository;
import ru.tsc.kitaev.tm.model.Session;

import javax.persistence.EntityNotFoundException;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class SessionRepository extends AbstractRepository implements ISessionRepository {

    @Override
    public void add(@NotNull Session session) {
        entityManager.persist(session);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM Session")
                .executeUpdate();
    }

    @Override
    public @Nullable Session findById(@NotNull String id) {
        return entityManager
                .createQuery("FROM Session WHERE id = :id", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(findById(id));
    }

}
